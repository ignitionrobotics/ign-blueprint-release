# ign-blueprint-release

The ign-blueprint-release repository has moved to: https://github.com/ignition-release/ign-blueprint-release

Until May 31st 2020, the mercurial repository can be found at: https://bitbucket.org/osrf-migrated/ign-blueprint-release
